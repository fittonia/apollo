#include "apollo.h"

#include <stdlib.h>
#include <time.h>

typedef float color_t[4];
color_t default_bgcolor = { 0.1f, 0.3f, 0.4f, 1.0f };

typedef struct {
	wlr_output_t *wlr_output;
	wl_listener_t destroy;
	wl_listener_t frame;
} output_t;

static void output_frame_notify(wl_listener_t *listener, void *data) {
	wlr_output_t *wlr_output = data;
	wlr_renderer_t *renderer = wlr_backend_get_renderer(wlr_output->backend);

	wlr_output_attach_render(wlr_output, NULL);

	printf("new frame !\n");

	int width, height;
	wlr_output_effective_resolution(wlr_output, &width, &height);
	wlr_renderer_begin(renderer, width, height);

	wlr_renderer_clear(renderer, default_bgcolor);

	render_each_shell_surface(renderer, wlr_output);

	wlr_output_render_software_cursors(wlr_output, NULL);

	wlr_renderer_end(renderer);
	wlr_output_commit(wlr_output);
}

static void output_destroy_notify(wl_listener_t *listener, void *data) {
	output_t *output = wl_container_of(listener, output, destroy);
	wl_list_remove(&output->frame.link);
	wl_list_remove(&output->destroy.link);
	free(output);

	wl_display_terminate(display);
}

static void new_output_notify(wl_listener_t *listener, void *data){
	wlr_output_t *wlr_output = data;

	printf("new output !\n");

	wlr_output_mode_t *mode = wlr_output_preferred_mode(wlr_output);
	if (mode) wlr_output_set_mode(wlr_output, mode);

	wlr_output_enable(wlr_output, true);
	wlr_output_layout_add_auto(output_layout, wlr_output);

	output_t *output = malloc(sizeof(output_t));
	output->wlr_output = wlr_output;

	output->destroy.notify = output_destroy_notify;
	wl_signal_add(&wlr_output->events.destroy, &output->destroy);
	output->frame.notify = output_frame_notify;
	wl_signal_add(&wlr_output->events.frame, &output->frame);

	wlr_output_commit(wlr_output);
}

wl_listener_t new_output = { .notify = new_output_notify };

void output_init(){
	wl_signal_add(&backend->events.new_output, &new_output);
}