#ifndef WAYLAND_H
#define WAYLAND_H

#include <wayland-server.h>

#include <wlr/types/wlr_xcursor_manager.h>
#include <wlr/types/wlr_output_layout.h>
#include <wlr/types/wlr_input_device.h>
#include <wlr/types/wlr_data_device.h>
#include <wlr/types/wlr_compositor.h>
#include <wlr/types/wlr_xdg_shell.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/render/wlr_texture.h>
#include <wlr/types/wlr_keyboard.h>
#include <wlr/types/wlr_output.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_cursor.h>
#include <wlr/types/wlr_seat.h>
#include <wlr/types/wlr_box.h>
#include <wlr/backend.h>

#define TYPEDEF(ident) typedef struct ident ident##_t;

// wayland

TYPEDEF(wl_event_loop)
TYPEDEF(wl_interface)
TYPEDEF(wl_listener)
TYPEDEF(wl_display)
TYPEDEF(wl_list)

// wlroots

TYPEDEF(wlr_event_pointer_motion_absolute)
TYPEDEF(wlr_event_pointer_motion)
TYPEDEF(wlr_event_pointer_button)
TYPEDEF(wlr_event_pointer_axis)
TYPEDEF(wlr_event_keyboard_key)
TYPEDEF(wlr_xcursor_manager)
TYPEDEF(wlr_output_layout)
TYPEDEF(wlr_xdg_toplevel)
TYPEDEF(wlr_input_device)
TYPEDEF(wlr_output_mode)
TYPEDEF(wlr_xdg_surface)
TYPEDEF(xkb_rule_names)
TYPEDEF(wlr_xdg_shell)
TYPEDEF(wlr_keyboard)
TYPEDEF(wlr_renderer)
TYPEDEF(wlr_session)
TYPEDEF(wlr_surface)
TYPEDEF(wlr_texture)
TYPEDEF(wlr_backend)
TYPEDEF(xkb_context)
TYPEDEF(xkb_keymap)
TYPEDEF(wlr_output)
TYPEDEF(wlr_cursor)
TYPEDEF(wlr_seat)
TYPEDEF(wlr_box)

#endif