#ifndef APOLLO_H
#define APOLLO_H

#include "typedef.h"

#include <stdlib.h>
#include <stdio.h>

extern wl_display_t *display;
extern wlr_backend_t *backend;
extern wlr_session_t *term_session;
extern wlr_output_layout_t *output_layout;

extern wlr_cursor_t *cursor;
extern wlr_seat_t *seat;

extern void output_init();
extern void shells_init();
extern void input_init();
extern void seat_init();

extern void shell_cursor_update(wlr_cursor_t *cursor, uint32_t time);
extern void render_each_shell_surface(wlr_renderer_t *renderer, wlr_output_t *wlr_output);

static const void *nnc(const void *e, unsigned id){
	if (!e){
		wlr_session_destroy(term_session);
		printf("Error %u\n", id);
		exit(1);
	}
	return e;
}

static void *nn(void *e, unsigned id){
	if (!e){
		wlr_session_destroy(term_session);
		printf("Error %u\n", id);
		exit(1);
	}
	return e;
}

#endif