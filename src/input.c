#include "apollo.h"

#include <stdlib.h>

typedef struct {
	wlr_input_device_t *device;
	wl_listener_t key;
	wl_listener_t modifiers;
} keyboard_t;

wlr_cursor_t *cursor;
wlr_xcursor_manager_t *cursor_mgr;

static void process_cursor_motion(uint32_t time){
	wlr_xcursor_manager_set_cursor_image(cursor_mgr, "left_ptr", cursor);
	shell_cursor_update(cursor, time);
}

static void cursor_motion(wl_listener_t *listener, void *data){
	wlr_event_pointer_motion_t *e = data;

	wlr_cursor_move(cursor, e->device, e->delta_x, e->delta_y);
	process_cursor_motion(e->time_msec);
}

static void cursor_motion_absolute(wl_listener_t *listener, void *data){
	wlr_event_pointer_motion_absolute_t *e = data;

	wlr_cursor_warp_absolute(cursor, e->device, e->x, e->y);
	process_cursor_motion(e->time_msec);
}

static void cursor_button(wl_listener_t *listener, void *data){
	wlr_event_pointer_button_t *e = data;

	wlr_seat_pointer_notify_button(seat, e->time_msec, e->button, e->state);
	// if (e->state == WLR_BUTTON_PRESSED) shell_cursor_update(cursor, 0);
}

static void cursor_axis(wl_listener_t *listener, void *data){
	wlr_event_pointer_axis_t *e = data;

	wlr_seat_pointer_notify_axis(seat, e->time_msec, e->orientation, e->delta, e->delta_discrete, e->source);
}

static void cursor_frame(wl_listener_t *listener, void *data){
	wlr_seat_pointer_notify_frame(seat);
}

static void keyboard_handle_modifiers(wl_listener_t *listener, void *data) {
	keyboard_t *kb = wl_container_of(listener, kb, modifiers);

	wlr_seat_set_keyboard(seat, kb->device);
	wlr_seat_keyboard_notify_modifiers(seat, &kb->device->keyboard->modifiers);
}

static void keyboard_handle_key(struct wl_listener *listener, void *data){
	keyboard_t *keyboard = wl_container_of(listener, keyboard, key);
	wlr_event_keyboard_key_t *event = data;

	wlr_seat_set_keyboard(seat, keyboard->device);
	wlr_seat_keyboard_notify_key(seat, event->time_msec, event->keycode, event->state);
	
	const xkb_keysym_t *syms;
	xkb_state_key_get_syms(keyboard->device->keyboard->xkb_state, event->keycode + 8, &syms);
	if (syms[0] == XKB_KEY_Escape) wl_display_terminate(display);
}

static void new_keyboard(wlr_input_device_t *device){
	xkb_rule_names_t rules = { 0 };
	xkb_context_t *context = nn(xkb_context_new(XKB_CONTEXT_NO_FLAGS), 6);
	xkb_keymap_t *keymap = xkb_map_new_from_names(context, &rules, XKB_KEYMAP_COMPILE_NO_FLAGS);
	wlr_keyboard_set_keymap(device->keyboard, keymap);
	xkb_keymap_unref(keymap);
	xkb_context_unref(context);

	wlr_keyboard_set_repeat_info(device->keyboard, 25, 600);

	keyboard_t *keyboard = malloc(sizeof(keyboard_t));
	keyboard->device = device;

	keyboard->modifiers.notify = keyboard_handle_modifiers;
	wl_signal_add(&device->keyboard->events.modifiers, &keyboard->modifiers);

	keyboard->key.notify = keyboard_handle_key;
	wl_signal_add(&device->keyboard->events.key, &keyboard->key);

	wlr_seat_set_keyboard(seat, device);
}

static void new_pointer(wlr_input_device_t *device){
	wlr_cursor_attach_input_device(cursor, device);
}

static void new_input(wl_listener_t *listener, void *data) {
	wlr_input_device_t *device = data;
	uint32_t caps = seat->capabilities;

	printf("new input !\n");

	/*__*/ if (device->type == WLR_INPUT_DEVICE_KEYBOARD){
		new_keyboard(device);
		caps |= WL_SEAT_CAPABILITY_KEYBOARD;
	} else if (device->type == WLR_INPUT_DEVICE_POINTER){
		new_pointer(device);
		caps |= WL_SEAT_CAPABILITY_POINTER;
	}

	wlr_seat_set_capabilities(seat, caps);
}

void input_init(){
	cursor = wlr_cursor_create();
	cursor_mgr = wlr_xcursor_manager_create(NULL, 24);

	wlr_cursor_attach_output_layout(cursor, output_layout);
	wlr_xcursor_manager_load(cursor_mgr, 1);

	static wl_listener_t cursor_motion_l          = { .notify = cursor_motion };
	static wl_listener_t cursor_motion_absolute_l = { .notify = cursor_motion_absolute };
	static wl_listener_t cursor_button_l          = { .notify = cursor_button };
	static wl_listener_t cursor_axis_l            = { .notify = cursor_axis };
	static wl_listener_t cursor_frame_l           = { .notify = cursor_frame };

	wl_signal_add(&cursor->events.motion,          &cursor_motion_l);
	wl_signal_add(&cursor->events.motion_absolute, &cursor_motion_absolute_l);
	wl_signal_add(&cursor->events.button,          &cursor_button_l);
	wl_signal_add(&cursor->events.axis,            &cursor_axis_l);
	wl_signal_add(&cursor->events.frame,           &cursor_frame_l);

	static wl_listener_t new_input_l = { .notify = new_input };
	wl_signal_add(&backend->events.new_input, &new_input_l);
}