#include "apollo.h"

wlr_seat_t *seat;

static void seat_request_cursor(struct wl_listener *listener, void *data) {
	struct wlr_seat_pointer_request_set_cursor_event *event = data;
	struct wlr_seat_client *focused_client = seat->pointer_state.focused_client;

	if (focused_client == event->seat_client){
		wlr_cursor_set_surface(cursor, event->surface, event->hotspot_x, event->hotspot_y);
	}
}

static void seat_request_set_selection(struct wl_listener *listener, void *data){
	struct wlr_seat_request_set_selection_event *event = data;

	wlr_seat_set_selection(seat, event->source, event->serial);
}

void seat_init(){
	seat = wlr_seat_create(display, "seat0");
	
	static wl_listener_t request_cursor = { .notify = seat_request_cursor };
	static wl_listener_t request_set_selection = { .notify = seat_request_set_selection };

	wl_signal_add(&seat->events.request_set_cursor, &request_cursor);
	wl_signal_add(&seat->events.request_set_selection, &request_set_selection);
}