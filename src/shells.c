#include "apollo.h"

typedef struct {
	wlr_xdg_surface_t *xdg_surface;

	wl_listener_t map;
	wl_listener_t unmap;
	wl_listener_t destroy;

	bool mapped;
	bool resized;
	double x, y;

	wl_list_t link;
} surface_t;

// 3 structs for surfaces: surface, wlr_surface and wlr_xdg_surface
typedef enum {
	SYM_SURFACE,
	WLR_SURFACE,
	XDG_SURFACE
} surface_struct_t;

typedef union {
	surface_t         *sym;
	wlr_surface_t     *wlr;
	wlr_xdg_surface_t *xdg;
} surface_union_t;

wl_list_t surfaces;

typedef struct {
	wlr_output_t *output;
	wlr_renderer_t *renderer;
	surface_t *surface;
	struct timespec *when;
} render_data_t;

void maximize_surface(surface_t *surface, wlr_output_t *wlr_output){
	wlr_xdg_toplevel_set_maximized(surface->xdg_surface, true);

	wlr_box_t *box = wlr_output_layout_get_box(output_layout, wlr_output);

	wlr_xdg_toplevel_set_size(surface->xdg_surface, box->width, box->height);
	surface->x = box->x;
	surface->y = box->y;
	surface->resized = true;
}

surface_union_t get_hovered_surface(wlr_cursor_t *cursor, double *sx, double *sy, surface_struct_t sct){
	double cx = cursor->x;
	double cy = cursor->y;

	surface_union_t ret;

	surface_t *surface;
	wl_list_for_each(surface, &surfaces, link){
		double _sx = cx - surface->x;
		double _sy = cy - surface->y;

		wlr_surface_t *_s = wlr_xdg_surface_surface_at(surface->xdg_surface, _sx, _sy, sx, sy);
		if (_s){
			/**/ if (sct == WLR_SURFACE) ret.wlr = _s;
			else if (sct == SYM_SURFACE) ret.sym = surface;
			else if (sct == XDG_SURFACE) ret.xdg = surface->xdg_surface;

			return ret;
		}
	}

	ret.wlr = NULL;
	return ret;
}

static void render_surface(struct wlr_surface *wlr_surface, int sx, int sy, void *data){
	render_data_t *rdata = data;
	surface_t *surface = rdata->surface;
	wlr_output_t *wlr_output = rdata->output;

	if (!surface->resized){
		maximize_surface(surface, wlr_output);
		// return; // wait for the surface to process resize
	}

	wlr_texture_t *texture = wlr_surface_get_texture(wlr_surface);
	if (texture == NULL) return;

	double ox = 0, oy = 0;
	wlr_output_layout_output_coords(output_layout, wlr_output, &ox, &oy);
	ox += surface->x + sx,
	oy += surface->y + sy;

	double scale = wlr_output->scale;
	wlr_box_t box = {
		.x      = scale * ox,
		.y      = scale * oy,
		.width  = scale * wlr_surface->current.width,
		.height = scale * wlr_surface->current.height,
	};

	float matrix[9];
	enum wl_output_transform transform = wlr_output_transform_invert(wlr_surface->current.transform);
	wlr_matrix_project_box(matrix, &box, transform, 0, wlr_output->transform_matrix);

	wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1);
	wlr_surface_send_frame_done(wlr_surface, rdata->when);
}

void render_each_shell_surface(wlr_renderer_t *renderer, wlr_output_t *wlr_output){
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC, &now);

	surface_t *surface;
	wl_list_for_each_reverse(surface, &surfaces, link){
		if (!surface->mapped) continue;
		render_data_t rdata = {
			.output = wlr_output,
			.surface = surface,
			.renderer = renderer,
			.when = &now,
		};
		wlr_xdg_surface_for_each_surface(surface->xdg_surface, render_surface, &rdata);
	}
}

static void surface_input_update(surface_t *surface, double sx, double sy, uint32_t time){
	static surface_t *prev_surface = NULL;

	// if (prev_surface != surface){
	// 	if (prev_surface) wlr_xdg_toplevel_set_activated(prev_surface->xdg_surface, false);
	// 	prev_surface = surface;
	// }
	/* Move the view to the front */
	// wl_list_remove(&surface->link);
	// wl_list_insert(&surfaces, &surface->link);

	wlr_xdg_surface_t * xdg_surface = surface->xdg_surface;
	wlr_xdg_toplevel_set_activated(xdg_surface, true);

	if (time){
		wlr_seat_pointer_notify_motion(seat, time, sx, sy);
		wlr_seat_pointer_notify_enter(seat, xdg_surface->surface, sx, sy);
	}

	wlr_keyboard_t *kb = wlr_seat_get_keyboard(seat);
	wlr_seat_keyboard_notify_enter(seat, xdg_surface->surface, kb->keycodes, kb->num_keycodes, &kb->modifiers);
}

void shell_cursor_update(wlr_cursor_t *cursor, uint32_t time){
	double sx, sy;
	surface_t *surface = get_hovered_surface(cursor, &sx, &sy, SYM_SURFACE).sym;
	if (surface) surface_input_update(surface, sx, sy, time);
	else wlr_seat_pointer_clear_focus(seat);
}

static void xdg_surface_map(wl_listener_t *listener, void *data){
	surface_t *surface = wl_container_of(listener, surface, map);
	surface->mapped = true;
	surface->resized = false;

	wl_list_insert(&surfaces, &surface->link);
	surface_input_update(surface, 0, 0, 0);
}

static void xdg_surface_unmap(wl_listener_t *listener, void *data){
	surface_t *surface = wl_container_of(listener, surface, unmap);
	surface->mapped = false;
}

static void xdg_surface_destroy(wl_listener_t *listener, void *data){
	surface_t *surface = wl_container_of(listener, surface, destroy);
	wl_list_remove(&surface->link);
	free(surface);
}

static void new_xdg_surface(wl_listener_t *listener, void *data){
	wlr_xdg_surface_t *xdg_surface = data;

	if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) return;

	surface_t *surface = malloc(sizeof(surface_t));
	surface->xdg_surface = xdg_surface;
	surface->x = 300;
	surface->y = 300;
	surface->mapped = false;
	surface->resized = false;

	surface->map.notify = xdg_surface_map;
	surface->unmap.notify = xdg_surface_unmap;
	surface->destroy.notify = xdg_surface_destroy;

	wl_signal_add(&xdg_surface->events.map, &surface->map);
	wl_signal_add(&xdg_surface->events.unmap, &surface->unmap);
	wl_signal_add(&xdg_surface->events.destroy, &surface->destroy);
}

void shells_init(){
	wl_list_init(&surfaces);

	wlr_xdg_shell_t *xdg_shell = wlr_xdg_shell_create(display);
	static wl_listener_t new_xdgs = { .notify = new_xdg_surface };
	wl_signal_add(&xdg_shell->events.new_surface, &new_xdgs);
}