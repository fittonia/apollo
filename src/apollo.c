#include "apollo.h"

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

typedef struct {
	struct wlr_backend backend;
	struct wlr_session *session;
} wlr_multi_backend_t;

wl_display_t *display;
wlr_backend_t *backend;
wlr_session_t *term_session;
wlr_output_layout_t *output_layout;

void display_init(){
	display = nn(wl_display_create(), 1);

	backend = nn(wlr_backend_autocreate(display, NULL), 2);
	term_session = ((wlr_multi_backend_t *)backend)->session;
	wlr_renderer_t *renderer = wlr_backend_get_renderer(backend);
	wlr_renderer_init_wl_display(renderer, display);

	// necessary to get surfaces working:
	wlr_compositor_create(display, renderer);
	wlr_data_device_manager_create(display);

	output_layout = wlr_output_layout_create();
	nn(output_layout, 5);
}

void sighandler(int sig){
	wlr_session_destroy(term_session);
	printf("Signal %i\n", sig);
	exit(1);
}

#define SPAWN(cmd) if (fork() == 0) execl("/bin/sh", "/bin/sh", "-c", cmd, (void *)NULL);

int main(){

	signal(SIGSEGV, sighandler);
	signal(SIGINT, sighandler);

	display_init();
	output_init();
	shells_init();
	input_init();
	seat_init();

	nn((void *)wlr_backend_start(backend), 3);

	const char *socket = nnc(wl_display_add_socket_auto(display), 4);
	printf("WAYLAND_DISPLAY=%s\n", socket);

	setenv("WAYLAND_DISPLAY", socket, true);
	SPAWN("firefox")

	wl_display_run(display);

	wlr_session_destroy(term_session);
	wl_display_destroy(display);
	return 0;
}